# Version 1.0
# TR translation 2015-04-21

------------------------------------------------------------------------

# Gözetime karşı e-posta öz savunma

Kitlesel gözetim temel haklarımızı ihlal eder ve ifade özgürlüğüne bir tehdittir!

Ancak kendimizi savunabiliriz.

# Sorun

E-postalarınızı koruyan parola, gizli servisler tarafından kullanılan kitlesel gözetim teknolojilerine karşı koruma sağlamaz.

İnternet üzerinden gönderilen e-postalar, hedefine gidene kadar bir çok bilgisayar sistemi üzerinden geçer. Gizli servisler ve gözetim kuruluşları bunu bir avantaj olarak kullanarak her gün milyonlarca ve milyonlarca e-postayı okurlar.

Gizleyecek hiç bir şeyiniz olmadığını düşünseniz bile, korunmasız e-postalarla iletişim kurduğunuz herkesin açığa çıktığını unutmayın.

# Şifreleme

GnuPG kullanarak mahremiyetinizi geri alın! GnuPG e-postalarınızı gönderilmeden önce şifreler, böylece sadece istediğiniz kişiler onları okuyabilir.

GnuPG platform bağımsızdır. Bunun anlamı her e-posta adresiyle ve her çeşit bilgisayar veya mobil aygıtla çalışabileceğidir. GnuPG özgürdür ve ücretsizdir.

Binlerce insan, hem profesyonel hem de özel amaçlarla halihazırda GnuPG kullanıyor. Gelin ve bize katılın! Her bir kişi topluluğumuzu daha güçlü kılar ve direneceğimizi kanıtlar.

# Çözüm

GnuPG ile şifrelenmiş bir e-posta ele geçirildiğinde veya yanlış ellere geçtiğinde yararsızdır. Uygun gizli anahtar olmadan hiç kimse tarafından okunamaz. Sadece ve sadece niyetlenilen kişi tarafından tamamen normal bir e-posta gibi açılabilir.

Gönderici ve alıcı şimdi daha fazla güvendeler. Eğer özel bilgi içermeyen e-postalarınız olsa bile, tutarlı şifreleme kullanımı hepimizi haksız kitlesel gözetimden koruyacaktır.

# Özel e-posta iletişimi

# Mahremiyetinizi geri alın! GnuPG Kullanın!

- Özgür Yazılım
- bütün e-posta adresleri için
- GNU/Linux, Windows, Mac, Android, ...
- hesap oluşturma veya kayıt gerekmiyor
- ücretsiz

------------------------------------------------------------------------

# GnuPG nasıl çalışıyor

GnuPG şifrelemeyi kullanabilmek için biri açık biri gizli bir "anahtar" çifti oluşturuyorsunuz. Bu anahtarlar şu şekilde kullanılıyor:

# açık anahtar - şifreleme

Eğer birisi, size şifreli bir e-posta göndermek istiyorsa, sizin "açık anahtar"ınızı kullanması gerekiyor. Bu nedenle açık anahtarınızı ne kadar dağıtır/yayarsanız, o kadar iyi. 

Merak etmeyin. Açık anahtarınız sadece size gönderilecek e-postaları şifrelemek için kullanılabilir, şifrelerini çözmek için kullanılamaz.

# gizli anahtar - şifre çözme

"Gizli anahtar"ınız evinizin ön kapı anahtarı gibidir; onu kendi kişisel bilgisayarınızda güvende (ve gizli) tutarsınız. Ona erişimi olan tek kişinin siz olması gerektiğini dikkate alın!  

GnuPG'yi ve gizli anahtarınızı, size gönderilmiş olan bütün şifreli mesajları açık metin haline çevirmek ve okumak için kullanırsınız.

# GnuPG'yi güvenli kılan nedir? 

GnuPG **Özgür Yazılımdır** ve **Açık Standartları** kullanır. Bu, bir yazılımın bizi gözetimden gerçekten koruyabilmesinin asli unsurudur. Çünkü, özel mülkiyet yazılım ve biçimlerde olanlar sizin denetiminizin ötesine geçebilir.

Eğer bir programın kaynak kodunu hiç kimse göremezse, onun istenmeyen casus programlar ("arka kapılar") içermediğinden emin olamayız. Yazılımın nasıl çalıştığını göremiyorsak, ona ancak körlemesine güvenebiliriz.

Buna karşın Özgür Yazılımda temel koşul kaynak kodun yayınlanmasıdır. Özgür Yazılım, kaynak kodun herkes tarafından bağımsız denetimine ve kamusal gözden geçirmeye olanak sağlar ve destekler. Bu şeffaflıkla, arka kapılar tespit edilebilir ve kaldırılabilir.

Çoğu özgür yazılım, herkes için güvenli yazılımlar oluşturmak isteyen bir topluluğun elindedir. Eğer kendinizi gözetime karşı korumak istiyorsanız ancak Özgür Yazılıma güvenebilirsiniz.

# Özgür Yazılım nedir?

Özgür Yazılım herkes tarafından her amaçla kullanılabilir. Bu amaçlar içerisinde özgür kopyalama, kaynak kodu inceleme ve onu kendi ihtiyaçlarınıza göre geliştirme ve uyarlama ("dört özgürlük" denilen) yer alır.

Programı "yalnızca kullanmak" istiyorsanız bile, bu özgürlüklerden yararlanabilirsiniz. Çünkü bu özgürlükler Özgür Yazılımın toplumun elinde kalmasını ve daha fazla geliştirilmesinin özel şirketlerin veya hükümetlerin denetimine geçmemesini güvence altına alır. 

Bu konu hakkında ve Özgür Yazılımın bizi nasıl Özgür Topluma götürebileceğine ilişkin daha fazla bilgi için bakınız:

fsfe.org/freesoftware

# Pratik öneri

GnuPG'nin arkasındaki teknoloji birinci sınıf bir koruma sağlar. Aşağıdaki öneriler şifreli iletişiminizin başka nedenlerle tehlikeye girmesini önleyecektir:

E-postalarınızın şifresini çözmek için gizli anahtarınıza ve **parolaya** ihtiyacınız var. Bu parola en az 8 karakter uzunluğunda olmalı ve rakamlar, özel karakterlerin yanında küçük ve büyük harfler içermelidir. Ayrıca, sizin geçmişiniz hakkında bilgisi olan hiç kimse bu parolayı tahmin edememelidir.

**Gizli anahtarınızı yedekleyin!** Eğer sabit sürücünüz bozulursa yeni bir tane oluşturmak zorunda kalmayın, veri kaybına uğramayın.

**Mümkün olduğunca şifreleyin!** Böylece, başkalarının sizin ne zaman ve kiminle şifreli iletişim kurduğunuzu izlemelerini zorlaştırırsınız. Dolayısıyla, mesajlarınızı ne kadar çok şifrelerseniz, şifreli mesajlar o kadar az şüphe çekecektir.

**Konunun şifresiz gönderildiğini** unutmayın!

[This text is only used in languages that leave enough space to integrate it:] Her ne kadar e-posta içeriği şifrelense de, bütün tanımlayıcı veriler (alıcı, gönderen, tarih ve dosya boyutu) ele geçirilebilir.

# Öğrence

GnuPG ile e-posta öz savunma için bir öğrenceye şuradan erişebilirsiniz:

EmailSelfDefense.FSF.org/tr

Veya **"Kriptoparti"** adı verilen etkinlikleri izleyebilirsiniz. Bu etkinlikler insanların sizinle buluşup ücretsiz olarak GnuPG ve diğer şifreleme araçlarını kurma ve kullanmada size yardımcı oldukları etkinliklerdir.

Bu broşür, FSF ve Journalism++ tarafından hazırlanan şu sitedeki özgün grafiğin (CC BY 4.0) FSFE tarafından yeniden düzenlenmiş halidir: emailselfdefense.fsf.org

# FSFE hakkında

Bu broşür, kâr amacı gütmeyen, Özgür Yazılımı tanıtma ve özgür dijital bir toplum kurmaya adanmış Avrupa Özgür Yazılım Vakfı (FSFE) tarafından hazırlanmıştır.

Yazılıma erişim, toplumdaki katılımımızı belirler. Bu nedenle FSFE, dijital özgürlük için mücadele ederek, enformasyon çağında herkesin adil erişim ve katılımı için gayret eder.

Hiç kimse kendisine o **yazılımı kullanma, inceleme, paylaşma ve geliştirme** özgürlüklerini sunmayan bir yazılım kullanmaya zorlanmamalıdır. Teknolojiyi kendi gereksinimlerimize göre biçimlendirme hakkına ihtiyacımız var.

FSFE'nin çalışması, bu hedefleri benimseyen bir insan topluluğunca desteklenmektedir. Eğer bize katılmak ve/veya hedeflerimize ulaşmamıza yardımcı olmak istiyorsanız, katkı sunabileceğiniz bir çok yol var. Özgeçmişiniz ne olursa olsun. Bu konu hakkında daha fazla bilgiyi ve bize nasıl destek olacağınızı şuradan öğrenebilirsiniz:

fsfe.org/contribute

# Üyemiz olun!

Bağışlar çalışmamızı sürdürmemiz ve bağımsızlığımızı güvence altına almamız için çok önemlidir. Çalışmamızı en iyi FSFE üyesi olarak destekleyebilirsiniz. Bunu yaparak, bize Özgür Yazılım için nerede gerekiyorsa orada mücadeleye devam etmemiz için doğrudan yardımcı olmuş olursunuz.

fsfe.org/join

Bu ve diğer broşürleri ücretsiz bir şekilde talep edebilirsiniz:

fsfe.org/promo

Free Software Foundation Europe e.V.
Schönhauser Allee 6/7
10119 Berlin
Germany
https://fsfe.org

