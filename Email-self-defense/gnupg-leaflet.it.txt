# Version 1.0
# IT translation 1.2 (2015-04-13)
------------------------------------------------------------------------

# Autodifesa delle mail contro la sorveglianza

La sorveglianza di massa viola i nostri diritti fondamentali ed è una minaccia alla libertà di parola.

Ma: ci possiamo difendere.

# Il Problema

La password che ti consente di accedere alla tua posta elettronica non è sufficiente a proteggerla dalle tecnologie di sorveglianza di massa usate dai servizi segreti.

Ogni mail inviata tramite Internet passa attraverso molti computer prima di arrivare a destinazione. I servizi segreti e le agenzie di sorveglianza ne approfittano per leggere milioni e milioni di mail ogni giorno.

Anche se pensi di non avere nulla da nascondere: tutte le persone con cui comunichi attraverso mail non protette sono esposte allo stesso modo.

# Cifratura

Riconquista la tua privacy usando GnuPG! Questo sistema cifra le tue mail prima che vengano inviate, in modo da consentirne la lettura solo ai destinatari da te scelti.

GnuPG è indipendente dalla piattaforma. Ciò significa che può funzionare con qualsiasi indirizzo mail e praticamente su ogni computer o telefono cellulare recente. GnuPG è libero e gratuito.

Migliaia di persone usano già GnuPG per usi professionali e privati: fallo anche tu! Ogni persona rende la nostra comunità più forte e dimostra che teniamo alla nostra privacy.

# La Soluzione

Anche se una mail cifrata con GnuPG viene intercettata o finisce nelle mani sbagliate, non c'è problema: senza la chiave privata non può essere letta da nessuno. Per il destinatario previsto - e per lui solo -  si apre invece come una una normalissima mail.

In questo modo, mittente e destinatario sono entrambi al sicuro. Anche se alcune delle tue mail non contengono informazioni private, un uso costante della cifratura ci protegge dalla sorveglianza di massa ingiustificata.

# Comunicazione privata tramite mail

# Riconquista la tua privacy! Usa GnuPG!

- Software Libero
- per ogni indirizzo mail
- per GNU/Linux, Windows, Mac, Android...
- nessun account o registrazione necessaria
- gratuito

------------------------------------------------------------------------

# Come funziona GnuPG

Per usare la cifratura GnuPG devi creare una tua coppia di "chiavi", pubblica e privata, che viene usata così:

# chiave pubblica - cifra

Quando qualcuno vuole mandarti una mail cifrata, deve usare la tua "chiave pubblica". Quindi, più diffondi la tua chiave pubblica, meglio è.

Non ti preoccupare; la tua chiave pubblica può essere usata solo per cifrare mail indirizzate a te, non per decifrarle.

# chiave privata - decifra

La tua "chiave privata" è come la chiave della porta di casa tua; la tieni al sicuro sul tuo computer. Presta attenzione affinché tu sia l'unico ad accedervi.

Usa GnuPG e la tua chiave privata per decifrare e leggere tutte le mail cifrate che ti vengono inviate.

# Cosa rende sicuro GnuPG? 

GnuPG è **Software Libero** e usa **Standard Aperti**, aspetti essenziali per essere sicuri che il software sia in grado di proteggerci dalla sorveglianza. Infatti non hai modo di controllare cosa avviene all'interno di software e formati proprietari.

Se a nessuno è permesso esaminare il codice sorgente di un programma, non si può essere sicuri che non contenga programmi spia indesiderati - chiamati anche "backdoor". Se il software non rivela il suo funzionamento, siamo costretti a fidarci ciecamente.

Una delle condizioni fondamentali del Software Libero, invece, è che venga pubblicato il codice sorgente: il Software Libero consente e incoraggia il controllo indipendente e la verifica pubblica dei programmi. Grazie a questa trasparenza, le backdoor possono essere individuate e rimosse.

La maggior parte del Software Libero è nelle mani di una comunità che collabora al fine di realizzare software sicuro per tutti. Se ti vuoi proteggere dalla sorveglianza, puoi fare affidamento solo sul Software Libero.

# Cos'è il Software Libero?

Il Software Libero può essere usato da chiunque e per qualsiasi fine. Questo include la libertà di copiare e leggere il codice sorgente, e la possibilità di migliorarlo o adattarlo ai tuoi bisogni (le cosiddette "quattro libertà").

Anche se tu "vuoi solo usare" il programma, benefici comunque delle quattro libertà, in quanto garantiscono che il Software Libero rimanga nelle mani della società e che il successivo sviluppo non sia controllato dagli interessi di compagnie private o governi. 

Scopri come il Software Libero può portare a una società libera: 

fsfe.org/freesoftware

# Consigli pratici

La tecnologia dietro GnuPG fornisce una protezione di prima categoria. Le seguenti linee guida possono aiutarti a far sì che le tue comunicazioni cifrate non siano compromesse:

Per decifrare le tue mail hai bisogno della tua chiave privata e della tua **passphrase**. Essa deve essere lunga almeno otto caratteri e contenere numeri, caratteri speciali, minuscole e maiuscole; inoltre, nessuno con informazioni su di te deve essere in grado di indovinarla.

**Fai una copia della tua chiave privata!** In questo modo, se it tuo disco rigido si rompe, non dovrai creare una nuova chiave e non perderai i tuoi dati.

**Cifra quanto più possibile!** Così facendo, eviterai che terzi sappiano quando e con chi ti scambi informazioni sensibili. Infatti, quanto più spesso cifri i tuoi messaggi, tanto meno sospetto apparirà un tuo messaggio cifrato.

Attenzione: l'**oggetto della tua mail non è cifrato**.

[This text is only used in languages that leave enough space to integrate it:] Anche quando il contenuto della tua mail viene cifrato, tutti i metadati corrispondenti (mittente, destinatario, data e dimensione del file) possono essere intercettati.

# Guida

Puoi trovare una semplice guida per difendere le tue mail con la cifratura GnuPG qui:

EmailSelfDefense.FSF.org

Oppure controlla se ci sono dei **"Criptoparty"** nella tua zona. A questi eventi puoi trovare persone pronte ad aiutarti a impostare e usare GnuPG e altri strumenti per la cifratura.

Questo volantino è un remix creato dalla FSFE sulla base delle immagini originali di FSF e Journalism++ disponibili su: emailselfdefense.fsf.org

# Cos'è la FSFE 

Questo volantino è stato creato dalla Free Software Foundation Europe (FSFE), un'organizzazione no-profit che si dedica a promuovere il Software Libero e lavora per realizzare una società digitale libera.

L'accesso al software determina come siamo coinvolti nella società. La FSFE si impegna per garantire un accesso equo e la partecipazione di tutti nell'era dell'informazione, combattendo per le libertà digitali.

Nessuno dovrebbe essere obbligato ad usare software che non garantisca le libertà di **usare, studiare, condividere e migliorare il software**. Vogliamo il diritto di modificare la tecnologia per soddisfare i nostri bisogni.

Il lavoro della FSFE è il risultato degli sforzi di una comunità di persone impegnate a battersi per questi valori. Se vuoi unirti a noi e aiutarci a raggiungere questi obbiettivi puoi contribuire in molti modi mettendo semplicemente a disposizione le tue abilità e conoscenze. Per saperne di più:

fsfe.org/contribute

# Diventa un membro sostenitore

Le donazioni sono importanti affinché la FSFE possa continuare il suo lavoro restando indipendente. Puoi supportarci diventando un "Fellow", un membro sostenitore della FSFE.
In questo modo ci aiuterai a combattere per il Software Libero ogni volta che ce ne sarà bisogno.

fsfe.org/join

Puoi ordinare questo volantino gratuitamente:

fsfe.org/promo

Free Software Foundation Europe e.V.
Schönhauser Allee 6/7
10119 Berlin
Germany
https://fsfe.org


