# Version 1.0

------------------------------------------------------------------------

# Itsepuolustusta sähköpostin valvontaa vastaan

Joukkovalvonta loukkaa perusoikeuksiamme ja on uhka sananvapaudelle!

Mutta: me voimme puolustautua.

# Ongelma

Sähköpostisi salasana ei ole riittävä suoja viesteillesi salaisten palveluiden käyttämiä joukkovalvontateknologioita vastaan.

Jokainen Internetin kautta lähetettävä sähköposti kulkee usean tietokoneen kautta matkallaan määränpäähänsä. Salaiset palvelut ja vakoiluvirastot käyttävät tätä hyväkseen lukiessaan miljoonittain sähköposteja joka päivä.

Vaikka olisitkin sitä mieltä ettei sinulla ole mitään salattavaa: jokainen, joka kommunikoi kanssasi salaamattomilla sähköposteilla, altistuu myös vakoilulle.

# Salaus

Turvaa yksityisyytesi GnuPG:llä! Se salaa sähköpostisi ennen niiden lähettämistä, jotta vain valitsemasi vastaanottaja voi lukea ne.

GnuPG on alustariippumaton. Se tarkoittaa, että se toimii jokaisella sähköpostiosoitteella ja lähes kaikissa tietokoneissa sekä uusimmissa puhelimissa. GnuPG on vapaa ja saatavilla ilmaiseksi.

Jo tuhannet ihmiset käyttävät GnuPG:tä työssään ja vapaa-ajallaan. Tule ja liity meihin! Jokainen henkilö tekee yhteisöstämme vahvemman ja todistaa, että olemme valmiita puolustautumaan.

# Ratkaisu

Aina kun GnuPG:llä salattu sähköposti siepataan tai se päätyy vääriin käsiin, se on hyödytön: ilman oikeaa yksityistä avainta sitä ei voi lukea kukaan. Mutta, viestin oikealle vastaanottajalle - ja vain hänelle - se aukeaa kuin mikä tahansa normaali sähköposti.

Lähettäjä ja vastaanottaja ovat näin paremmassa turvassa. Ja vaikka jotkin viestisi eivät sisältäisikään yksityistä informaatiota, salauksen johdonmukainen käyttö suojaa meitä kaikkia perusteettomalta joukkovalvonnalta.

# Yksityinen sähköpostiviestintä

# Turvaa yksityisyytesi! Käytä GnuPG:tä!

- Vapaa ohjelmisto
- kaikille sähköpostiosoitteille
- GNU/Linuxille, Windowsille, Macille, Androidille ...
- ei käyttäjätiliä tai rekisteröintiä
- ilmainen

------------------------------------------------------------------------

# Miten GnuPG toimii

Käyttääksesi GnuPG:tä sinä luot erityisen parin "avaimia", julkisen ja yksityisen. Näitä avaimia käytetään seuraavasti:

# julkinen avain - salaus

Kun joku haluaa lähettää sinulle salatun sähköpostin, hänen on käytettävä "julkista avaintasi". Eli mitä useammalle jaat julkista avaintasi, sen parempi.

Älä huoli: sinun julkisella avaimellasi voidaan vain salata sinulle lähetettäviä sähköposteja, ei purkaa niiden salausta.

# yksityinen avain - purku

Sinun "yksityinen avaimesi" on kuin ulko-ovesi avain; se on henkilökohtainen ja se pidetään turvassa omalla tietokoneellasi. Varmista, että vain sinä voit käyttää sitä!

Käytät GnuPG:tä ja yksityistä avaintasi purkamaan kaikki sinulle lähetetyt salatut sähköpostiviestit jotta voit lukea ne.

# Mikä tekee GnuPG:stä turvallisen? 

GnuPG on **Vapaa ohjelmisto** ja se käyttää **Avoimia standardeja**. Se on välttämätöntä jotta voidaan varmistaa, että ohjelmisto todellakin voi suojella meitä valvonnalta, sillä suljetut ohjelmistot ja tiedostomuodot voivat toimia tahdostasi riippumatta ja sitä vastaan.

Jos kenenkään ei anneta nähdä ohjelman lähdekoodia, niin kukaan ei voi olla varma ettei ohjelma sisällä ei-haluttuja vakoiluohjelmia - niin kutsuttuja "taka-ovia". Jos ohjelma ei paljasta sitä miten se toimii, niin voimme ainoastaan luottaa siihen sokeasti.  

Sitä vastoin, yksi Vapaan ohjelmiston perusehdoista on sen lähdekoodin julmaiseminen: Vapaa ohjelmisto sallii ja kannustaa kaikkia lähdekoodin yksityiseen ja julkiseen arviointiin. Tällaisen läpinäkyvyyden ansiosta taka-ovet voidaan tunnistaa ja poistaa.

Suurin osa Vapaista ohjelmistoista on yhteisön käytettävissä, joka pyrkii yhdessä rakentamaan turvallisia ohjelmistoja kaikille. Jos haluat suojella itseäsi valvonnalta, voit luottaa ainoastaan Vapaisiin ohjelmistoihin.

# Mikä on Vapaa ohjelmisto?

Vapaita ohjelmistoja voidaan käyttää kenen tahansa toimesta mihin tahansa tarkoitukseen. Se tarkoittaa, että niitä voi vapaasti kopioida, niiden lähdekoodi on luettavissa ja niitä voidaan parantaa sekä muokata erilaisiin tarkoituksiin sopiviksi (ns. "neljä vapautta").

Vaikka haluaisitkin "vain käyttää" ohjelmia, niin hyödyt silti näistä vapauksista. Ne takaavat että Vapaat ohjelmistot säilyvät yhteiskuntamme käsissä ja että niiden kehittämistä eivät hallitse yksityisten yritysten tai hallitusten intressit. 

Lisää tietoa tästä ja siitä miten Vapaat ohjelmistot voivat johdattaa meidät vapaaseen yhteiskuntaan saat täältä:

fsfe.org/freesoftware

# Käytännön neuvoja

GnuPG:n takana oleva teknologia tarjoaa ensiluokkaisen suojauksen. Seuraavat ohjeet auttavat sinua varmistamaan että salattu viestintäsi ei murru muista syistä:

Purkaaksesi salatut postisi tarvitset yksityisen avaimesi sekä **salasanan**. Salasanan tulisi olla vähintään kahdeksan merkkiä pitkä ja siinä tulisi olla numeroita, erikoismerkkejä sekä pieniä ja isoja kirjaimia. Lisäksi, sen pitäisi olla sellainen ettei kenenkään sinut tuntevan pitäisi pystyä arvaamaan sitä.

**Varmuuskopioi yksityinen avaimesi!** Jos kiintolevysi hajoaa niin sinun ei tarvitse luoda uutta avainta etkä menetä vanhoja postejasi.

**Salaa mahdollisimman paljon!** Tekemällä niin estät muita huomaamasta milloin ja kenen kanssa vaihdat arkaluontoisia tietoja. Siis, mitä useammin salaat viestisi, sitä vähemmän epäilyttäviksi salatut viestit netissä tulevat.

Muista että viestin **aihe-kenttää ei salata!**

[This text is only used in languages that leave enough space to integrate it:] Vaikka itse viestisi sisältö salataankin, kaikki siihen liittyvä metadata (lähettäjä, vastaanottaja, päiväys, ja koko) voidaan kaapata.

# Kurssi

Löydät yksinkertaisen kurssin sähköpostin itsepuolustuksesta GnuPG:n salauksen avulla täältä:

EmailSelfDefense.FSF.org

Tai voit käydä ns. **"Kryptobileissä"** alueellasi. Ne ovat tapahtumia joissa voit tavata ihmisiä, jotka auttavat sinua mielellään GnuPG:n sekä muiden salaustyökalujen asentamisessa ja käytössä ilmaiseksi.

Tämä esite on FSFE:n tekemä rinnakkaisversio FSF:n ja Journalism++:n alkuperäisestä versiosta, joka on saatavilla täältä: emailselfdefense.fsf.org

# FSFE - vapauden asialla

Tämän lehtisen on tehnyt Free Software Foundation Europe (FSFE), joka on voittoa tavoittelematon organisaatio. FSFE omistautuu Vapaiden ohjelmistojen puolesta kampanjointiin ja vapaan digitaalisen yhteiskunnan rakentamiseen.

Kyky käyttää ohjelmistoja määrittää sen miten voimme ottaa osaa yhteiskunnan toimintaan. FSFE haluaa varmistaa, että kaikilla on tähän yhtäläiset mahdollisuudet taistelemalla digitaalisten vapauksien puolesta.

Ketään ei saa pakottaa käyttämään ohjelmistoa jota ei voi vapaasti **käyttää, tutkia, jakaa ja kehittää**. Meillä täytyy olla oikeus muokata teknologiaa tarpeisiimme sopivaksi.

FSFE:n saavutukset kumpuavat näille tavoitteille omistautuneen yhteisön työstä. Jos haluat tulla mukaan ja auttaa meitä saavuttamaan ne, sinulla on monia mahdollisuuksia osallistua taidoistasi riippumatta. Saat lisätietoja tästä sekä työmme tukemisesta alta:

fsfe.org/contribute

# Tue työtämme!

Lahjoitukset ovat tärkeitä työmme jatkuvuuden kannalta ja ne varmistavat että järjestömme pysyy riippumattomana. Liittymällä Kannattajajäsen-ohjelmaamme, tuet ja autat meitä jatkamaan taistelua digitaalisten vapauksien puolesta:

fsfe.org/join

Voit tilata tämän ja muita lehtisiä ilmaiseksi täältä:

fsfe.org/promo

Free Software Foundation Europe e.V.
Schönhauser Allee 6/7
10119 Berlin
Germany
https://fsfe.org

