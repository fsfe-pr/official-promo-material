# Version 1.0

------------------------------------------------------------------------

# E-posti enesekaitse lausjälgimise vastu
 

Massiline lausjälgimine on meie põhiõiguste vastane ja ohtlik sõnavabadusele!

Aga me saame ennast kaitsta.

# Probleem

Salasõna, mis kaitseb sinu postkasti, ei kaitse sinu e-kirju salateenistuste kasutuses olevate lausjälgimise tehnoloogiate eest.  

Iga interneti kaudu saadetud e-kiri läbib oma sihtpunkti jõudmiseks mitmeid arvutisüsteeme. Salateenistused ja järelevalveametid kasutavad seda olukorda ära, et lugeda miljoneid e-kirju päevas. 

Isegi kui sa arvad, et sul pole midagi varjata: igaüks, kellega sa suhtled turvamata e-kirjade kaudu, on lausjälgimise ohu all. 


# Krüpteerimine

Saa tagasi oma privaatsus, kasutades GnuPG-d! See krüpteerib sinu e-kirjad enne ärasaatmist, nii et ainult sinu valitud adressaadid saavad neid lugeda.   

GnuPG on platvormist sõltumatu. See tähendab, et see töötab iga e-posti aadressiga ja igal arvutil või nutitelefonil. GnuPG on vabalt ja tasuta kättesaadav. 

Tuhanded inimesed juba kasutavad GnuPG-d kas tööalaselt või isiklikul eesmärgil. Tule ja ühine meiega! Iga uus GnuPG kasutaja teeb meie kogukonna tugevamaks ja tõestab meie vastupanuvõimet. 

# Lahendus

Iga GnuPG kaudu krüpteeritud e-kiri, mis on kinni peetud või sattunud valedesse kätesse, on kasutu: seda ei ole võimalik lugeda ilma vajaliku võtmeta. Kuid see avaneb nagu täiesti tavaline e-kiri õige saaja ja ainult tema jaoks.  

Nüüd on mõlemad, nii saatja kui ka saaja, kaitstud. Isegi kui mõned sinu e-kirjad ei sisalda isiklikku infot, kaitseb järjepidev krüpteerimine meid kõiki õigustamatu lausjälgimise eest.  

# Privaatne suhtlus e-posti kaudu

# Võta tagasi oma privaatsus! Kasuta GnuPG-d!

- Vaba tarkvara
- Töötab kõigi e-posti aadressidega...
- ja arvutisüsteemidel nagu GNU/Linux, Windows, Mac, Android
- ilma konto loomiseta või registreerimiseta
- tasuta

------------------------------------------------------------------------

# Kuidas GnuPG töötab

Selleks, et kasutada GnuPG krüpteerimist, tuleb luua ainulaadne võtmepaar, mis koosneb avalik- ja privaatvõtmest. Need võtmed on vajalikud järgmiste toimingute teostamiseks:

# avalik võti - krüpteeri

Selleks, et saata sulle krüpteeritud kiri, on saatjal vaja teada sinu "avalikku võtit". Seega mida rohkem sa oma avalikku võtit jagad, seda parem.  

Ära muretse: sinu avalik võti on vajalik ainult selleks, et krüpteerida sinule saadetud e-kirju, mitte selleks, et neid avada. 

# privaatvõti - dekrüpteeri

Sinu "privaatvõti" on nagu sinu maja esiukse võti; sina hoiad seda turvalises (ja privaatses) kohas oma isiklikus arvutis, ja üksnes sinul peaks sellele juurdepääs olema! 

Sina kasutad GnuPG-d ja oma privaatvõtit selleks, et dekrüpteerida ja lugeda kõiki sinule saadetud krüpteeritud kirju.  

# Mis tagab GnuPG turvalisuse?

GnuPG on **vaba tarkvara** ja põhineb **avatud standarditel**. See on hädavajalik, et olla kindel selles, et tarkvara tõepoolest kaitseb meid jälgimise eest. Suletud tarkvara ja suletud vormingute puhul ei ole meil kontrolli selle üle, mis juhtuda võib. 

Kui kellelgi pole õigust näha arvutiprogrammi lähtekoodi, ei saa keegi olla kindel ka selles, et selles arvutiprogrammis puuduvad soovimatud nuhkimisprogrammid ehk nn "tagauksed". Kui arvutiprogramm varjab seda, kuidas see töötab, saame me vaevalt seda pimesi usaldada.  

Vastupidi vabale tarkvarale, mille aluspõhimõtteks on avalikustatud lähtekood: vaba tarkvara lubab avalikkusel oma lähtekoodi iseseisvalt kontrollida. Selline läbipaistvus tagab selle, et võimalikud tagauksed leitakse ja eemaldatakse. 

Enamik vaba tarkvara kuulub kogukonnale, mis töötab üheskoos selle kallal, et tarkvara oleks kõigi jaoks turvaline. Kui sina tahad ennast jälgimise eest kaitsta, siis usalda ainult vaba tarkvara.  

# Mis on vaba tarkvara

Igaüks saab kasutada vaba tarkvara ükskõik millisel eesmärgil: vabaks kopeerimiseks, lähtekoodi lugemiseks, selle parendamiseks ja oma vajaduste jaoks kohandamiseks (niinimetatud "neli vabadust").  

Isegi siis, kui sina tahad üksnes "kasutada" programmi, saad sa ikkagi kasu kõigist neljast vabadusest, sest need tagavad, et vaba tarkvara jääb kogu meie ühiskonna kasutusse ja selle edaspidine areng ei ole erafirmade või valitsuste kontrolli all.  

Uuri rohkem selle kohta, kuidas vaba tarkvara tagab meile vaba ühiskonna:   

fsfe.org/freesoftware

# Praktiline soovitus

GnuPG tehnoloogia tagab esmaklassilise kaitse. Allolevad juhised aitavad su krüpteeritud suhtlusel jääda mööndusteta privaatseks.

Et dekrüpteerida oma e-kirju, on sul vaja oma privaatvõtit ja **salasõna**. See salasõna peaks olema vähemalt 8 ühikut pikk ja sisaldama arve, märke, suur- ja väiketähti. Lisaks sellele ei tohiks mitte keegi sinust piisava taustainformatsiooni evimise korral olema võimeline seda salasõna ära arvama.   

**Tee oma privaatvõtmest varukoopia!** Juhul, kui su kõvaketas enam ei tööta, ei pea sa looma uut võtit ja sa ei kaota oma andmeid.  

**Krüpteeri nii palju kui võimalik!** Nii jääb teistele teadmatuks, millal ja kellega sa jagad tundlikku informatsiooni. Ehk mida sagedamini sa krüpteerid oma sõnumeid, seda vähem kahtlust krüpteeritud sõnumid tekitavad.

Pea meeles, et e-kirja **pealkiri jääb krüpteerimata**!

[This text is only used in languages that leave enough space to integrate it:] Vaatamata sellele, et su e-kirja sisu on krüpteeritud, on võimalik kõiki muid metaandmeid (saatja, saaja, kuupäev ja faili suurus) siiski koguda.

# Juhised

Lihtne juhend e-posti enesekaitseks GnuPG krüpteerimise abil on kättesaadav siin: 

EmailSelfDefense.FSF.org

Või uuri nn "krüpto-pidude" toimumise kohta oma naabruskonnas. Selle raames saad tutvuda inimestega, kes rõõmuga aitavad sul seadistada nii GnuPG-d kui ka muid krüpteerimisseadmeid täiesti tasuta. 

Käesolev voldik on FSFE remiks, mis põhineb FSF ja Journalism++ (CC BY 4.0) originaalgraafikal. Kättesaadav: emailselfdefense.fsf.org

# FSFE-st

Käesolev voldik on loodud mittetulundusorganisatsiooni Free Software Foundation Europe (FSFE) poolt, mis on asutatud vaba tarkvara ja vaba digitaalühiskonna edendamiseks.  

Juurdepääs tarkvarale otsustab seda, kuidas me ühiskonnast osa võtame. Sellest tulenevalt võitleb FSFE selle poolt, et kõik saaks infoajastust osa võtta ja sellele juurde pääseda, st FSFE seisab digitaalvabaduse eest. 

Mitte keegi ei peaks olema sunnitud kasutama tarkvara, mis ei anna vabadust seda "kasutada", sellest "õppida", seda "jagada" ja seda "edendada". Meil peab olema õigus kohandada tehnoloogiat vastavalt meie vajadustele. 

FSFE tegevuse taga on kogukond, mis on pühendunud nende eesmärkide saavutamisele. Kui sa tahad meiega ühineda, siis selleks on mitmeid võimalusi. Pole oluline, milline on sinu taust. Uuri rohkem meist ja sellest, kuidas meid toetada, siit:  

fsfe.org/contribute

# Astu liikmeks ja toeta meid!

Toetused aitavad meil jätkata meie tööd, jäädes samal ajal iseseisvaks. Kõige paremini saab seda teha, astudes meie toetavaks liikmeks ehk "Vennaskonda". Niiviisi aitad meil otseselt jätkata võitlust vaba tarkvara poolt, olenemata asukohast.   

fsfe.org/join

Käesolevat voldikut ja muud materjali saad tasuta tellida siit:

l.fsfe.org/promo

Free Software Foundation Europe e.V.
Schönhauser Allee 6/7
10119 Berliin 
Saksamaa 
https://fsfe.org

