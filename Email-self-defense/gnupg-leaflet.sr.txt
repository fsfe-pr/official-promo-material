# Version 1.0

------------------------------------------------------------------------

# Samozaštita od imejl nadzora

Masovni nadzor krši naša osnovna prava i predstavlja pretnju slobodi govora!

Ali, možemo se zaštititi.

# Problem

Lozinka koja štiti vaš imejl nalog nije dovoljna da zaštiti vašu elektronsku poštu od tehnologije masovnog nadzora koju koriste tajne službe.

Svaki imejl poslat preko interneta prođe kroz mnogo kompjuterskih sistema pre nego što stigne tamo gde je namenjen. Tajne službe i agencije za nadzor koriste ovu činjenicu kako bi čitali milione i milione imejlova svakoga dana.

Čak i ako mislite da vi nemate šta da krijete, svi ostali koji komuniciraju sa vama preko nezaštićenih imejlova su takođe izloženi ovoj opasnosti.

# Šifrovanje

Uzmite natrag svoju privatnost korišćenjem programa GnuPG! Ovaj program šifruje vaš imejl pre nego što ga pošaljete, tako da samo vi i primalac kome je namenjen možete da ga pročitate.

GnuPG ne zavisi od platforme koju koristite. To znači da radi na svim imejl adresama i funkcioniše na praktično svim računarima ili novijim mobilnim telefonima. GnuPG je besplatan i dostupan svima bez ikakve naknade.

Hiljade ljudi već koriste GnuPG, bilo poslovno ili privatno. Pridružite nam se! Svaka nova osoba čini našu zajednicu jačom i pokazuje da smo spremni da uzvratimo udarac.

# Rešenje

Kada god se imejl šifrovan programom GnuPG presretne, ili završi u pogrešnim rukama, beskoristan je – ne može ga pročitati niko bez odgovarajućeg privatnog ključa. Ali, osoba kojoj je namenjen (i samo ona) otvara ga kao sasvim običan imejl.

Na taj način su i pošiljalac i primalac bezbedniji. Čak i ako vaši imejlovi ne sadrže nikakve lične podatke, stalna upotreba šifrovanja štiti sve nas od neopravdanog masivnog nadzora.

# Privatna prepiska preko imejla

# Uzmite natrag svoju privatnost! Koristite GnuPG!

- Slobodan softver
- za sve imejl adrese
- za GNU/Linux, Windows, Mac, Android, ...
- ne zahteva registraciju ni otvaranje naloga
- besplatno

------------------------------------------------------------------------

# Kako GnuPG funkcioniše

Da bi koristili GnuPG šifrovanje, stvarate jedinstveni par „ključeva” - privatni i javni. Oni se koriste na sledeći način:

# javni ključ - šifrovanje

Kada neko želi da vam pošalje šifrovani imejl, potrebno je da koriste vaš „javni ključ”. Dakle, što više delite svoj javni ključ – to bolje. 

Ne brinite: vaš javni ključ se može koristiti samo da šifruje vaše imejlove, ne i da ih dešifruje.

# privatni ključ - dešifrovanje

Vaš „privatni ključ” je kao ključ od vaše kuće – čuvate ga na bezbednom (i privatnom mestu) – na svom računaru. Postarajte se da jedino vi imate pristup svom privatnom ključu!

Koristite GnuPG i svoj privatni ključ da dešifrujete i čitate sve imejlove koji vam stignu.

# Šta čini GnuPG tako bezbednim? 

GnuPG je **slobodan softver** i koristi **otvorene standarde**. Ovo je ključno kako bi bili sigurni da softver zaista može da nas zaštiti od nadzora, pošto se u vlasničkom softveru i formatima mogu desiti stvari izvan naše kontrole.

Ako niko ne može da vidi izvorni kod programa, niko ne može biti siguran da on ne sadrži nepoželjne špijunske programe  - takozvane „backdoor” programe. Ako softver ne otkriva kako zaista funkcioniše, jedino što možemo je da mu slepo verujemo.

Nasuprot tome, osnovni uslov slobodnog softvera jeste da objavi svoj izvorni kod – slobodni softver svima omogućava i podržava nezavisno proveravanje i javni prikaz korišćenog izvornog koda. Uz ovakvu transparentnost, „backdoor” programi mogu se uočiti i ukloniti.

Veći deo slobodnog softvera leži u rukama zajednice koja se trudi da omogući bezbedan softver za sve. Ako želite da se zaštitite od nadzora, možete verovati jedino slobodnom softveru.

# Šta je slobodan softver?

Slobodni softver može koristiti bilo ko u bilo koje svrhe. To podrazumeva besplatno umnožavanje, čitanje izvornog koda i mogućnost da se on poboljša ili prilagodi sopstvenim potrebama (to su takozvane četiri slobode).

Čak iako „samo želite da koristite” program, možete imati koristi od ovih sloboda, zato što one garantuju da slobodni softver ostaje u rukama našeg društva i da njegov dalji razvoj ne kontrolišu interesi privatnih kompanija ili vlada. 

Saznajte više o ovome i kako slobodni softver može da dovede do slobodnog društva: 

fsfe.org/freesoftware

# Praktični saveti

Tehnologija koja stoji iza GnuPG pruža vrhunsku zaštitu. Sledeće smernice pomoći će vam da se osigurate da vaša šifrovana komunikacija nije zloupotrebljena u druge svrhe:

Kako biste dešifrovali svoje imejlove potrebni su vam privatni ključ i **lozinka**. Lozinka bi trebalo da ima najmanje 8 karaktera, da sadrži cifre, simbole, kao i velika i mala slova. Osim toga, niko ko ima podatke o vama ne bi trebalo da može da pogodi vašu lozinku.

**Napravite „backup” svog privatnog ključa!** Ako vam se hard disk pokvari, ne morate praviti novi, a nećete ni izgubiti podatke.

**Šifrujte što više možete!** Na taj način, sprečavate druge da saznaju kada i sa kim razmenjujete delikatne informacije. Prema tome, što češće šifrujete poruke, to će šifrovane poruke biti manje sumnjive.

Imajte u vidu da se **naslov poruke ne šifruje!**

[This text is only used in languages that leave enough space to integrate it:] Iako se sadržaj vaših imejlova šifruje, svi odgovarajući metapodaci (pošaljilac, primalac, datum i veličina dokumenta) i dalje mogu biti otkriveni.

# Uputstva

Jednostavna uputstva za samozaštitu imejlova GnuPG šifrovanjem možete pronaći ovde:  

EmailSelfDefense.FSF.org

Ili, pronađite takozvane **Kriptožurke** u svom kraju. Na ovakvim dešavanjima možete upoznati ljude koji će vam rado pomoći da podesite i besplatno koristite GnuPG, kao i druge alate za šifrovanje.

Ovaj letak je obrada organizacije FSFE zasnovana na originalnoj grafici FSF i Journalism++, koja je dostupna na adresi emailselfdefense.fsf.org

# O organizaciji FSFE

Ovaj letak napravila je Fondacija za slobodni softver Evrope, neprofitna organizacija posvećena promovisanju slobodnog softvera koja radi na stvaranju slobodnog digitalnog društva.

Pristup softveru određuje na koji način možemo da učestvujemo u zajednici. Stoga FSFE, boreći se za digitalnu slobodu, teži poštenom pristupu i učešću za svakoga u dobu informacija.

Niko nikada ne bi trebalo da bude primoran da koristi softver koji ne pruža slobodu **korišćenja, proučavanja, deljenja i poboljšavanja softvera**. Treba da imamo pravo da oblikujemo tehnologiju kako bi odgovarala našim potrebama.

Rad FSFE zasniva se na zajednici ljudi posvećenih ovim ciljevima. Ako biste želeli da nam se pridružite i/ili pomognete nam da ostvarimo ciljeve, postoji mnogo načina na koje možete doprineti. Bez obzira na to odakle dolazite. Više o ovome i o načinima na koje možete podržati naš rad, možete saznati klikom na link:

fsfe.org/contribute

# Postanite trajni član!

Donacije su ključan element koji omogućava naš rad i garantuje našu nezavisnost. Naš rad najbolje možete podržati tako što ćete postati trajni član FSFE, „Saradnik”. 
Na taj način, direktno nam pomažete da nastavimo borbu za slobodni softver gde god je to potrebno.

fsfe.org/join

Možete besplatno naručiti ovaj i druge letke:

fsfe.org/promo

Free Software Foundation Europe e.V.
Schönhauser Allee 6/7
10119 Berlin
Germany
https://fsfe.org

