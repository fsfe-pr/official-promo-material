﻿Zerwij kajdany
==============

Jest wiele urządzeń i multimediów, które zachowują naszą wolność.
Możemy wybrać życie bez cyfrowych kajdanek. Możemy kupować multimedia, których
wolno nam potem używać bez końca, w dowolnym, wybranym przez nas
formacie. Nie trzeba długo szukać, by znaleźć urządzenia, które nie
traktują nas jak swoich niewolników.

Od strony politycznej, musimy zadecydować, czy system praw autorskich
ma służyć tylko wydawcom, czy także całemu społeczeństwu i jego przyszłym
pokoleniom. Zamiast zwalczać wszelkie rodzaje cyfrowego kopiowania, powinniśmy
wspierać modele biznesowe, które szanują nasze fundamentalne prawa do wolności
i prywatności. Musimy stworzyć system praw autorskich, na którym skorzystają
wszyscy, a nie tylko kilka firm.

Pozbycie się przepisów, które ograniczają wykorzystanie legalnie zakupionych treści
to ważny pierwszy krok.

**Więcej informacji** o **DRM** i wolności w cyfrowym świecie:

* Defective by design

    https://defectivebydesign.org/

* drm.info

    http://drm.info/

* Wikipedia

    https://pl.wikipedia.org/wiki/Zarządzanie_prawami_cyfrowymi

* EDRi

    https://edri.org/search/DRM

# Blue frame
Ten tekst został napisany przez pracowników i wolontariuszy FSFE oraz EDRi.

Free Software Fundation Europe jest organizacją pozarządową skupiającą się
na promowaniu wolnego oprogramowania i pracy nad budową wolnego społeczeństwa
cyfrowego.

Digitale Gesellschaft e.V to niemieckie stowarzyszenie, zaangażowane w obronę
praw obywatelskich i konsumenckich w kontekście polityki internetu. 

Ten dokument jest udostępniony na licencji Creative Commons (CC-BY-SA 4.0)

Ilustracje na okładce: CC BY-SA 3.0
Brendan Mruk oraz Matt Lee

Ilustracja „warning triangle”: CC BY-SA 2.5
AlfredoDanielRezinovsky

DRM
===

Dziwaczny, popsuty świat Zarządzania Ograniczeniami Cyfrowymi

Chciałbyś oglądać swoje **legalnie zakupione filmy** na różnych urządzeniach?
Chciałbyś tworzyć **kopie zapasowe** swoich płyt DVD? A może chciałbyś
przekonwertować swoje książki elektroniczne do **innych formatów**?

Systemy zarządzania prawami cyfrowymi **ograniczają Twoje prawo** do robienia
wszystkich tych rzeczy. Twoje filmy lub e-booki mogą nawet **przestać
działać**, jeśli usługodawca zbankrutuje lub przestanie wspierać ustalony
system DRM.
