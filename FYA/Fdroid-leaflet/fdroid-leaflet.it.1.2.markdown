<!--- Free Your Android ­- Leaflet
Italian Translation, v1.2
Updated on 2015-09-14 by Nicola Feltrin -->

<!--- Front -->
# Una questione di libertà, non di prezzo
Non devi pagare per le applicazioni di F-Droid. Trovi molte applicazioni gratuite anche su Google Play o Apple App Store. Tuttavia, Free Software non è una questione di prezzo ma di libertà.

Quando non sei tu a controllare un programma è lui che controlla te. Di conseguenza chiunque controlli il programma controlla te.

Per esempio, a nessuno è permesso studiare come funzioni un’applicazione non-libera, e cosa effettivamente faccia sul tuo telefono. Alle volte semplicemente non fa esattamente ciò che vorresti, ma ci sono applicazioni con caratteristiche nocive che ad esempio comunicano i tuoi dati a terzi senza il tuo esplicito consenso.

Usare esclusivamente Software Libero sul tuo telefono ti permette di essere in totale controllo. Anche se non hai la conoscenza necessaria per esercitare tutte e quattro le libertà, beneficerai del sapere di una vivace comunità basata su libertà e collaborazione.

Potresti dimostrare la tua riconoscenza pagando volontariamente gli sviluppatori. Questo ti mette nella posizione di essere un cliente e non un prodotto.

# Cos’è il Software Libero?

1. **Uso:** Libertà di eseguire il programma, per qualsiasi scopo.
2. **Studio:** Libertà di studiare come funziona il programma e di modificarlo in modo da adattarlo alle proprie necessità.
3. **Condivisione:** Libertà di ridistribuire copie in modo da aiutare il prossimo.
4. **Miglioramento:** Libertà di migliorare il programma e distribuirne pubblicamente i miglioramenti da voi apportati, in modo tale che tutta la comunità ne tragga beneficio.

**Perchè un programma sia veramente Software Libero, deve garantire queste quattro libertà a chiunque.**

# Cos’è la FSFE

Questo volantino è stato creato dalla Free Software Foundation Europe (FSFE), un’organizzazione no-profit che si dedica a promuovere il Software Libero e lavora per realizzare una società digitale libera.

L’accesso al software determina come siamo coinvolti nella società. La FSFE si impegna per garantire un accesso equo e la partecipazione di tutti nell'era dell'informazione, combattendo per le libertà digitali.

Nessuno dovrebbe essere obbligato ad usare software che non garantisca le libertà di **usare, studiare, condividere e migliorare il software**. Vogliamo il diritto di modificare la tecnologia per soddisfare i nostri bisogni.

Il lavoro della FSFE è il risultato degli sforzi di una comunità di persone impegnate a battersi per questi valori. Se vuoi unirti a noi e aiutarci a raggiungere questi obbiettivi puoi contribuire in molti modi mettendo semplicemente a disposizione le tue abilità e conoscenze. Per saperne di più:

fsfe.org/contribute

# Sostieni il nostro lavoro

Le donazioni sono importanti affinché la FSFE possa continuare il suo lavoro restando indipendente. Puoi supportarci diventando un "Fellow", un membro sostenitore della FSFE. In questo modo ci aiuterai a combattere per il Software Libero ogni volta che ce ne sarà bisogno.

fsfe.org/join

Ordina questo ed altri volantini l.fsfe.org/promo

2015-09-14

Free Software Foundation Europe e.V.
Schönhauser Allee 6/7 · 10119 Berlin · Germany

https://fsfe.org

# Libera il tuo Android!
## Installa un App Store libero per il tuo Android

Usa App Libere
- Senza Pubblicità
- Senza Spionaggio
- Senza Monitoraggio
- Senza Account Google

<!--- Back -->
# Un App Store libero

_Il logo di F-Droid_

F-Droid non è il tipico store dove compri qualcosa. Invece, è un catalogo (o repository) di applicazioni di Software Libero. Puoi facilmente navigare, cercare e installare applicazioni nel tuo dispositivo. F-Droid controlla anche gli aggiornamenti per te.

_La schermata principale di F-Droid_

# Installare F-Droid

Prima di installare F-Droid, assicurati di aver autorizzato l’installazione di applicazioni non ufficiali.

Puoi autorizzarlo in:
Impostazioni → Sicurezza → Origini sconosciute

Dopo aver scaricato F-Droid, sulla sinistra della schermata trovi i permessi di cui F-Droid ha bisogno e ti verrà chiesto di autorizzarli. I permessi sono necessari affinché F-Droid funzioni.

Per installare F-Droid usa il codice QR che trovi sulla sinistra o inserisci il seguente indirizzo nel tuo browser.

https://f-droid.org/FDroid.apk

# Usare F-Droid

In alto trovi tre schede: una per le applicazioni disponibili, una per quelle installate e una per i possibili aggiornamenti. Sotto trovi un’opzione per filtrare le applicazioni per categoria.

Quando selezioni un’applicazione, ti appare una schermata dove trovi la descrizione e le diverse versioni disponibili. La versione raccomandata è evidenziata con una stella. Cliccando sulla riga della versione potrai scaricare e installare l’applicazione.

Premendo il tasto o, a seconda del tuo dispositivo, il bottone menù (angolo in alto a destra) verrà visualizzata una schermata con opzioni addizionali come per esempio la possibilità di donare. Anche agli sviluppatori di applicazioni libere piace mangiare, quindi per favore considera una donazione.

_Schermata dei dettagli in Firefox_

# Alcune App Disponibili

**K-9 Mail** è la migliore App per email per Andrid. Supporta account multipli, cartelle e centinaia di altre opzioni come per esempio cifratura delle mail.

**OsmAnd~** è come Google Maps, ma con le mappe di OpenStreetMap e la possibilità di consultarle disconnessi dalla rete. Così puoi avere con te tutte le tue mappe anche quando non hai accesso a internet.

**Liberario** è un aiuto per spostarsi con i mezzi pubblici, trova coincidenze, stazioni vicine e prossime partenze. Supporta anche preferiti e mappe.

**FBreader** è un lettore di e-book. Ti permette anche di scaricare libri da librerie online come il Project Gutenberg.

**Xabber** è come Google Talk o ICQ. Ti permette di chattare con i tuoi amici, ma allo stesso tempo di scegliere il tuo fornitore di servizi tra una vasta gamma di organizzazioni e compagnie.

**Twidere** è l’App che ti serve se sei un micro-blogger e usi servizi come Status.Net o Twitter.

**AntennaPod** ti permette di iscriverti e ascoltare registrazioni audio da internet, i cosiddetti podcast.

**Slight Backup** è una versatile App per il backup. Salva i tuoi SMS, il registro delle chiamate, le impostazioni, i contatti e molto altro sulla tua scheda SD.

# Libera il tuo telefono

Anche se Android è principalmente Software Libero, il tuo dispositivo Android normalmente ti viene fornito con software proprietario e servizi che ti negano la possibilità di usarlo in maniera indipendente ed autonoma.

Vuoi un dispositivo mobile che non ti spii? La campagna della FSFE “Libera il tuo Android” raccoglie informazioni su come far girare un sistema operativo Android il più libero possibile e prova a coordinare gli sforzi in quest’area:

https://FreeYourAndroid.org

**CyanogenMod** gira su molti dispositivi senza un account Google. Anche se è generalmente più libero di molti dei programmi forniti con il tuo dispositivo, la libertà non è il suo obbiettivo principale. Sempre più componenti non-libere vengono aggiunte.

**Replicant** non scende a compromessi sulla libertà. Questa è l’opzione migliore, se hai un telefono che può funzionare senza driver proprietari.

# Contribuisci a F-Droid

L’iniziativa F-Droid è interamente sviluppata e mantenuta da volontari. Anche tu puoi contribuire in vari modi: segnala problemi, traduci le App F-Droid, aggiungine di nuove o aiuta a svilupparne.

La comunità è amichevole, ospitale, e ti sarà riconoscente per il tuo contributo. Non esitare, veni a conoscerci. Puoi trovare ulteriori informazioni all’indirizzo:

https://f-droid.org/contribute
