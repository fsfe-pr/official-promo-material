# Porównanie

W przeciwieństwie do innych rodzajów oprogramowania, Wolne Oprogramowanie
zapewnia nam podstawowe wolności. Większość z nas przywykła do poważnych
ograniczeń cyfrowych, dlatego też analogia do powszechnych, fizycznych narzędzi
powinna pomóc.

Wolne Oprogramowanie może być użyte jako narzędzie. Wyobraźmy sobie, jak
absurdalne konsekwencje miałoby używanie śrubokrętów, gdyby nadać im takie
same ograniczenia, jakie posiada niewolne oprogramowanie:

--------------------------------------------------------------------------------

1. Śrubokręty nie byłyby standaryzowane i działałyby tylko ze śrubkami
wykonanymi przez tego samego producenta.

2. Licencje śrubokrętów pozwalałby tylko na odkręcanie śrubek. Wkręcenie śrubki
wymagałoby zakupu droższego śrubokrętu tego samego producenta.

3. Tylko jedna firma mogłaby produkować śrubokręty, ze względu na to, że
posiadałaby na nie patenty. By zapewnić, przykładowo, powszechne prawo do
użytku kombinerek w celu dokręcania lub odkręcania śrubek, niezbędny byłby
długi i kosztowny proces sądowny.

4. Nie mielibyśmy możliwości modyfikacji śrubokrętów, aby dostosować je do
własnych potrzeb. Zabronione byłoby na przykład oklejenie rączki taśmą, żeby
zapobiec wyślizgiwaniu się śrubokrętu z ręki.

5. Naprawienie zepsutego śrubokręta bez zgody producenta byłoby nielegalne. Tak
samo jak rozebranie go na części, żeby zbadać w jaki sposób działa.

6. Na rynku wciąż pojawiałyby się nowe śrubokręty i śrubki, niekompatybilne ze
starszymi modelami. Musielibyśmy ciągle kupować nowe śrubokręty, aby obsługiwać
nowe urządzenia.

7. Pożyczenie lub odsprzedaż śrubokręta byłyby nielegalne.

8. Śrubokręty szpiegowałyby nas potajemnie i wysyłały podstępnie zebrane
informacje do osób trzecich (lub firm, czy rządów państw), całkowicie bez
naszej wiedzy.

--------------------------------------------------------------------------------

Nikt nie używałby śrubokręta, na który zostały nałożone takie ograniczenia.
Nie ma żadnych rozsądnych podstaw, by zaakceptować takie warunki, 
zarówno w przypadku narzędzi jak i oprogramowania.

Wolności, których naturalnie wymagamy w przypadku narzędzi, są też możliwe do
uzyskania w świecie komputerów. Wolne programy mogą być zainstalowane bez
żadnych obaw. Dzięki temu, że Wolne Oprogramowanie opiera się na Otwartych
Standardach, Twoje pliki mogą być otworzone różnymi programami nawet po wielu
latach.

Z drugiej strony, patenty na oprogramowanie, Cyfrowe Zarządzanie Ograniczeniami
(DRM), zamknięte standardy, licencje własnościowego oprogramowanie i nieuczciwe
Warunki Użytkowania (Terms of Service) to sposoby na ograniczenie wolności
użytkowników.

--------------------------------------------------------------------------------

# Gdzie wykorzystuje się Wolne Oprogramowanie?

---
**W komputerach biurowych i laptopach:**
Istnieje ogromna ilość Wolnego Oprogramowania przeznaczona do wykonywania Twoich zadań, projektów i codziennego użytku. Oto kilka przykładów:

**GNU/Linux** to wolny system operacyjny. Znajdziesz go w różnych wariantach,
zwanych dystrybucjami. Niektóre z nich nadają się dla początkujących
użytkowników, inne zostały stworzone, aby spełniać konkretne profesjonalne
wymagania.

**Mozilla Firefox, VLC, Audacity,** i **LibreOffice** to tylko niektóre
przykłady powszechnie używanych wolnych programów. Jest ich znacznie
więcej.

---

**Na innych urządzeniach:**
Noszenie przy sobie kilku komputerów naraz jest dziś dość powszechne. Nazywamy
je "smartphone'ami", tabletami albo czytnikami e-booków; ale tak naprawdę są to
komputery, często bardzo ograniczone.

**Telefony z Androidem** są przykładem urządzeń, które da się uwolnić.
Więcej informacji znajdziesz na https://freeyourandroid.org


**Na wszystkich komputerach, wszędzie wokół nas:**
Na co dzień mamy do czynienia z niezliczoną liczbą elektronicznych przedmiotów
sterowanych przez komputery, a co za tym idzie przez oprogramowanie. W
niektórych z tych urządzeń wciąż używa się własnościowego oprogramowania. Na
szczęście, Wolne Oprogramowanie pokrywa dużą część oprogramowania wbudowanego.

piekarniki, odtwarzacze CD, zmywarki
telewizory, samochody, projektory
systemy grzewcze, pociągi, zdalnie sterowane helikoptery...

Również **infrastruktura Internetu**, tak samo jak i sama "sieć", w znacznej
większości opiera się na Wolnym Oprogramowaniu.

modemy, rutery, VPN
serwery DNS, bazy danych, serwery sieciowe
serwery pocztowe, oprogramowanie e-commerce...
